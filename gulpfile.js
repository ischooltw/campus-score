var gulp = require('gulp');
var replace = require('gulp-replace');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var connect = require('gulp-connect');
var shell = require('gulp-shell');
var minifyInline = require('gulp-minify-inline');

gulp.task('build', function() {
    gulp.src('temp/5844D1D1-E9DF-47C6-A914-5F4D41B157B7/*')
        .pipe(gulp.dest('dist/5844D1D1-E9DF-47C6-A914-5F4D41B157B7'));

    gulp.src('campus-score-for-teacher.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/5844D1D1-E9DF-47C6-A914-5F4D41B157B7'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/5844D1D1-E9DF-47C6-A914-5F4D41B157B7'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/5844D1D1-E9DF-47C6-A914-5F4D41B157B7'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/5844D1D1-E9DF-47C6-A914-5F4D41B157B7'));

    gulp.src('temp/6AD9F56E-87C2-4654-A6C8-EB603911C6E0/*')
        .pipe(gulp.dest('dist/6AD9F56E-87C2-4654-A6C8-EB603911C6E0'));

    return gulp.src('campus-score-for-student.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/6AD9F56E-87C2-4654-A6C8-EB603911C6E0'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/6AD9F56E-87C2-4654-A6C8-EB603911C6E0'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/6AD9F56E-87C2-4654-A6C8-EB603911C6E0'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/6AD9F56E-87C2-4654-A6C8-EB603911C6E0'));
});

gulp.task('serve', function() {
    connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['serve', 'build'], shell.task([
    /^win/.test(require('os').platform()) ? 'start http://localhost:8080/' : 'open http://localhost:8080/'
]));
